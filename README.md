# COLAB by Manuel Ahumada

COLAB is a web-app that connects musicians from everywhere around the world to make music together. In here you'll be able to publish a track starting point, for others to finish in a collaboration type of agreement. 
As anyone can place new things on top of the track, you'll find several branches for the same song, with every edit creating a new one. After some time, the original user can choose a final branch, and start and agreement contract. Every musician involved in the song's branch will have the same legal share of the final piece. 
If wanted, the group can publish the song and possibly, with the community's recognition, be awarded with our songs of the month / year playlists; which will be featured on our main page.