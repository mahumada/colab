const mongoose = require('mongoose');
const { uuid } = require('uuidv4');

const userSchema = new mongoose.Schema({
  uuid: { type: String, default: uuid() },
  username: { type: String, required: true, unique: true },
  bio: { type: String, default: "No bio yet..." },
  pictureUrl: { type: String, default: "https://t4.ftcdn.net/jpg/00/64/67/63/360_F_64676383_LdbmhiNM6Ypzb3FM4PPuFP9rHe7ri8Ju.jpg" },
  songs: { type: Array, default: [] },
  date: { type: Date, default: Date.now }
});

const userModel = mongoose.model('User', userSchema);

module.exports = userModel;