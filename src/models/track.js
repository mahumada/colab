const mongoose = require('mongoose');
const { uuid } = require('uuidv4');

const trackSchema = new mongoose.Schema({
  uuid: { type: String, default: uuid() },
  parents: { type: Array, default: [] },
  description: { type: String, default: "No description provided..." },
  fileUrl: { type: String, required: true, unique: true }
});

const trackModel = mongoose.model('Track', trackSchema);

module.exports = trackModel;