const express = require('express');
const router = express.Router();

// Controller importing
const addUser = require('../controllers/user/add.js');
const getUsers = require('../controllers/user/getAll.js');
const getOneUser = require('../controllers/user/getOne.js');
const deleteUser = require('../controllers/user/delete.js');
const updateUser = require('../controllers/user/update.js');
const addSongToUser = require('../controllers/user/addSong.js');
const getUserSongs = require('../controllers/user/getSongs.js');

// Get requests
router.get('/', getUsers);
router.get('/:username', getOneUser);
router.get('/:username/songs', getUserSongs);

// Post request
router.post('/', addUser);

// Delete Request
router.delete('/:username', deleteUser);

// Put request
router.put('/:username', updateUser);
router.put('/:username/songs', addSongToUser);

module.exports = router;