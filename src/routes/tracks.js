const express = require('express');
const router = express.Router();

// Controller importing
const getAllTracks = require('../controllers/track/getAll.js');
const getTrack = require('../controllers/track/getOne.js')
const addTrack = require('../controllers/track/add.js');
const updateTrackDescription = require('../controllers/track/update.js');

// Get requests
router.get('/', getAllTracks);
router.get('/:uuid', getTrack);

// Post requests
router.post('/', addTrack);

// Put requrests
router.put('/:uuid', updateTrackDescription);

module.exports = router;