const express = require('express');
const router = express.Router();
const api = require('../controllers/api.js');

router.get('/', api);

module.exports = router;