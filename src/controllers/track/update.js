const Track = require('../../models/track.js');

module.exports = async(req, res) => {

  try {

    const updatedTrack = await Track.updateOne({ uuid: req.params.uuid }, { description: req.body.description });
    return res.status(200).json({ success: true, info: updatedTrack });

  } catch(error) {

    return res.status(409).json({ success: false, error });

  }

}