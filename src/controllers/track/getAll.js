const Track = require('../../models/track.js');

module.exports = async(req, res) => {

  try {

    const tracks = await Track.find({});
    return res.status(200).json(tracks);

  } catch(error) {

    return res.status(409).json({ success: false, error });

  }

}