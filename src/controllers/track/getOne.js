const Track = require('../../models/track.js');

module.exports = async(req, res) => {

  try {

    const track = await Track.findOne({ uuid: req.params.uuid });
    return res.status(200).json(track);

  } catch(error) {

    return res.status(409).json({ success: false, error });

  }

}