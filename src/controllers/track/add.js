const Track = require('../../models/track.js');
const User = require('../../models/user.js');

module.exports = async(req, res) => {

  const { username, description, fileUrl, parents } = req.body;

  // Check that username was provided
  if(!username){
    return res.status(409).json({ success: false, error: 'Track owner <username> not provided by http body...' });
  }

  try {

    // Add new track
    const newTrack = await Track.create({
      description,
      fileUrl,
      parents
    });
    // Add song uuid to user songs info
    const msg = await User.updateOne(
      { username }, 
      { $addToSet: { songs: newTrack.uuid } }
    );
    return res.status(200).json({ success: true, info: newTrack, info2: msg });

  } catch(error) {

    return res.status(409).json({ success: false, error });

  }

}