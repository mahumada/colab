const User = require('../../models/user.js');

module.exports = async(req, res) => {

  try {

    const msg = await User.deleteOne({ username: req.params.username });
    return res.status(200).json({ success: true, msg });
  
  } catch(error) {
    
    return res.status(409).json({ success: false, error });
  
  }
  

}