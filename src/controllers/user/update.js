const User = require('../../models/user.js');

module.exports = async(req, res) => {

  const { username, bio, pictureUrl } = req.body;

  try {
    
    const msg = await User.updateOne({ username: req.params.username }, { username, bio, pictureUrl })
    return res.status(200).json({ success: true, info: msg });

  } catch(error) {
    return res.status(409).json({ success: false, error });
  }

}