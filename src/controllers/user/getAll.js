const User = require('../../models/user.js');

module.exports = async(req, res) => {

  try {
    
    const users = await User.find({});
    return res.status(200).json(users);

  } catch(error) {

    return res.status(409).json({success: false, error});
  
  }

}