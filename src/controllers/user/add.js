const User = require('../../models/user.js');

module.exports = async(req, res) => {

  const { username, bio, pictureUrl } = req.body;

  try {

    const newUser = await User.create({
      username,
      bio,
      pictureUrl
    });
    const { uuid, songs, date } = newUser;    
    const newUserProtected = { uuid, username, bio, pictureUrl, songs, date };
    return res.status(200).json({ success: true, info: newUserProtected });

  } catch(error) {

    return res.status(409).json({success: false, error: error});  

  }
}