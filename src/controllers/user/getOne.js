const User = require('../../models/user.js');

module.exports = async(req, res) => {

  try {
    
    const user = await User.find({ username: req.params.username });
    return res.status(200).json({ user });

  } catch(error) {
  
    return res.status(409).json({success: false, error});
  
  }

}