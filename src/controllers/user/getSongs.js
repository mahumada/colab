const User = require('../../models/user.js');

module.exports = async(req, res) => {

  try {

    const user = await User.findOne({ username: req.params.username });
    return res.status(200).json( user.songs );

  } catch(error) {

    return res.status(409).json({ success: false, error });

  }

}