const User = require('../../models/user.js');

module.exports = async(req, res) => {

  try {

    const msg = await User.updateOne(
      { username: req.params.username }, 
      { $addToSet: { songs: req.body.song } }
    );
    return res.status(200).json({ success: true, info: msg });

  } catch(error) {

    return res.status(409).json({ success: false, error });

  }

}