const mongoose = require('mongoose');
const { Schema } = mongoose;

const User = require('../models/user.js');

// Database connection and testing
module.exports = async() => {
  await mongoose.connect(process.env.DB_STRING)
    .then(console.log('Database connection was succesful.'))
    .catch(err => { throw err });
}