// Library importing
const express = require('express');
const app = express();
const mongoose = require('mongoose');
require('dotenv').config();

// Define view engine
app.set('view engine', 'ejs');
app.set('views', './src/views');

// Json parser middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Express router routes
app.use('/', require('./routes/home.js'));
app.use('/api', require('./routes/api.js'));
app.use('/api/users', require('./routes/users.js'));
app.use('/api/tracks', require('./routes/tracks.js'));

// Start database
require('./config/database.js')();

// .env variables definition
const port = process.env.PORT || 3000;

// Not found page default response
app.use((req, res) => {
  res.status(404).render('notFound');
});

// Express server on listen
app.listen(port, () => {
  console.log(`Server listening on port ${port}...\nServer started at: ${new Date}.`);
});